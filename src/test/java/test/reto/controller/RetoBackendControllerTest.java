package test.reto.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import reto.controllers.RetoBackendController;
import reto.services.RetoBackendService;

public class RetoBackendControllerTest {

	@InjectMocks
	RetoBackendController controllerAPI;

	@Mock
	private RetoBackendService service;

	private MockMvc mockMvc;

	// private JacksonTester<RetoBackendModel> jsonRetoBackendModel;

	@BeforeEach
	public void setup() throws Exception {
		JacksonTester.initFields(this, new ObjectMapper());
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(controllerAPI).build();

	}

	@Test
	void testStatusOk() throws Exception {
		MockHttpServletResponse response = mockMvc.perform(post("/api/reto-backend")).andReturn().getResponse();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
	}

	@Test
	void testStatusNotFound() throws Exception {
		MockHttpServletResponse response = mockMvc.perform(post("/api/reto")).andReturn().getResponse();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
	}

	/*
	 * @Test void testStatusResponse() throws Exception { List<String> data = new
	 * ArrayList<String>(); data.add("1|Bluth|george.bluth@reqres.in");
	 * data.add("2|Weaver|janet.weaver@reqres.in");
	 * data.add("3|Wong|emma.wong@reqres.in");
	 * data.add("4|Holt|eve.holt@reqres.in");
	 * data.add("5|Morris|charles.morris@reqres.in");
	 * data.add("6|Ramos|tracey.ramos@reqres.in"); RetoBackendModel model = new
	 * RetoBackendModel(); model.setData(data);
	 * 
	 * MvcResult result = mockMvc.perform(post("/api/reto-backend")
	 * .characterEncoding("UTF-8") .header("key", "value")
	 * .contentType(MediaType.APPLICATION_JSON) .content("{ \"key\": \"value\" }"))
	 * .andDo(print()) .andExpect(status().isOk()) .andReturn();
	 * assertThat(result.getResponse().getContentAsString()).isEqualTo(
	 * jsonRetoBackendModel.write(model).getJson()); }
	 */
}
