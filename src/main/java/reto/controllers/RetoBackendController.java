package reto.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import reto.models.RetoBackendModel;
import reto.services.RetoBackendService;

@RestController
@RequestMapping("/api" + RetoBackendController.reto_backend)
public class RetoBackendController {

	public static final String reto_backend = "/reto-backend";

	@Autowired
	private RetoBackendService service;

	@PostMapping
	public RetoBackendModel reestructureData() {
		return service.dataRefactor();
	}

}
