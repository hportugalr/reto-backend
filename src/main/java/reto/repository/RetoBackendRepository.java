package reto.repository;

import org.springframework.stereotype.Repository;

import reto.models.RetoBackendModel;

@Repository
public interface RetoBackendRepository {

	RetoBackendModel dataRefactor();

}
