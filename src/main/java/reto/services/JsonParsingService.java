package reto.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class JsonParsingService {

	@Value("${endpoint.url}")
	private String endpointUrl;

	@Autowired
	private RestTemplate restTemplate;

	public Object parse() {
		return restTemplate.getForObject(endpointUrl, Object.class);

	}

}
