package reto.services;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import reto.dataEndpoint.PersonaDataEndpoint;
import reto.models.RetoBackendModel;
import reto.repository.RetoBackendRepository;

@Service
public class RetoBackendService implements RetoBackendRepository {

	@Autowired
	private JsonParsingService parsingService;

	public RetoBackendModel dataRefactor() {

		LinkedHashMap dataJson = (LinkedHashMap) parsingService.parse();

		ObjectMapper mapper = new ObjectMapper();
		List<PersonaDataEndpoint> listpersons = mapper.convertValue(dataJson.get("data"),
				new TypeReference<List<PersonaDataEndpoint>>() {
				});

		RetoBackendModel model = new RetoBackendModel();

		for (PersonaDataEndpoint person : listpersons)
			model.addData(person.getId(), person.getLast_name(), person.getEmail());

		return model;
	}

}
