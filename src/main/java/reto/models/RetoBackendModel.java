package reto.models;

import java.util.ArrayList;
import java.util.List;

public class RetoBackendModel {

	private List<String> data;

	public RetoBackendModel() {
		this.data = new ArrayList<String>();
	}

	public List<String> getData() {
		return data;
	}

	public void setData(List<String> data) {
		this.data = data;
	}

	public void addData(Long id, String last_name, String email) {
		String concatenate = id.toString() + "|" + last_name + "|" + email;
		this.data.add(concatenate);
	}

}
