package reto.models;

public class DataRetoBackendModel {

	private String resp;

	public DataRetoBackendModel(Long id, String last_name, String email) {
		super();
		this.resp = id.toString() + "|" + last_name + "|" + email;
	}

	public String getResp() {
		return resp;
	}

	public void setResp(String resp) {
		this.resp = resp;
	}

}